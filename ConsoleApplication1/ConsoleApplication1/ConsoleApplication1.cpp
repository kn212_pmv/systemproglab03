#include <windows.h>
#include <cwchar>
#include <tchar.h>
#include <lmcons.h>

void PrintDriveType(UINT driveType) {
    switch (driveType) {
    case DRIVE_UNKNOWN:
        printf("Тип: Невідомий\n");
        break;
    case DRIVE_NO_ROOT_DIR:
        printf("Тип: Без кореневого каталогу\n");
        break;
    case DRIVE_REMOVABLE:
        printf("Тип: Знімний\n");
        break;
    case DRIVE_FIXED:
        printf("Тип: Жорсткий диск\n");
        break;
    case DRIVE_REMOTE:
        printf("Тип: Мережевий\n");
        break;
    case DRIVE_CDROM:
        printf("Тип: Оптичний накопичувач\n");
        break;
    case DRIVE_RAMDISK:
        printf("Тип: RAM диск\n");
        break;
    default:
        printf("Тип: Невідомий\n");
    }
}
void GetSystemMemoryInfo() {
    MEMORYSTATUSEX memoryStatus;
    memoryStatus.dwLength = sizeof(memoryStatus);

    if (GlobalMemoryStatusEx(&memoryStatus)) {
        printf("Інформація про системну пам'ять:\n");
        printf("Фізична пам'ять: %llu КБ\n", memoryStatus.ullTotalPhys / 1024);
        printf("Вільна фізична пам'ять: %llu КБ\n", memoryStatus.ullAvailPhys / 1024);
        printf("Віртуальна пам'ять: %llu КБ\n", memoryStatus.ullTotalVirtual / 1024);
        printf("Вільна віртуальна пам'ять: %llu КБ\n", memoryStatus.ullAvailVirtual / 1024);
    }
    else {
        printf("Не вдалося отримати інформацію про системну пам'ять\n");
    }
}

void GetComputerNameInfo() {
    wchar_t computerName[MAX_COMPUTERNAME_LENGTH + 1];
    DWORD size = sizeof(computerName) / sizeof(computerName[0]);

    if (GetComputerNameW(computerName, &size)) {
        printf("Назва комп'ютера: %ls\n", computerName);
    }
    else {
        printf("Не вдалося отримати назву комп'ютера\n");
    }
}

void GetUserNameInfo() {
    wchar_t userName[UNLEN + 1];
    DWORD size = sizeof(userName) / sizeof(userName[0]);

    if (GetUserName(userName, &size)) {
        printf("Назва поточного користувача: %ls\n", userName);
    }
    else {
        printf("Не вдалося отримати назву поточного користувача\n");
    }
}

void GetDirectoryInfo() {
    wchar_t systemDir[MAX_PATH];
    wchar_t tempDir[MAX_PATH];
    wchar_t currentDir[MAX_PATH];

    if (GetSystemDirectory(systemDir, MAX_PATH) > 0) {
        printf("Поточний системний каталог: %ls\n", systemDir);
    }
    if (GetTempPath(MAX_PATH, tempDir) > 0) {
        printf("Тимчасовий каталог: %ls\n", tempDir);
    }
    if (GetCurrentDirectory(MAX_PATH, currentDir) > 0) {
        printf("Поточний робочий каталог: %ls\n", currentDir);
    }
    else {
        wprintf(L"Не вдалося отримати поточний системний каталог\n");
    }
}




int main() {
   
    DWORD drives = GetLogicalDrives();

    printf("Список усіх логічних дисків в системі:\n");

    for (int i = 0; i < 26; i++) {
        if (drives & (1 << i)) {
            char driveLetter = 'A' + i;
            printf("%c:\\\n", driveLetter);

            wchar_t rootPath[4] = { driveLetter, ':', L'\\', L'\0' };
            UINT driveType = GetDriveType(rootPath);
            PrintDriveType(driveType);

            ULARGE_INTEGER freeBytesAvailable, totalNumberOfBytes, totalNumberOfFreeBytes;
            if (GetDiskFreeSpaceEx(rootPath, &freeBytesAvailable, &totalNumberOfBytes, &totalNumberOfFreeBytes)) {
                printf("Вільне місце: %llu байт\n", freeBytesAvailable.QuadPart);
                printf("Загальна ємність: %llu байт\n", totalNumberOfBytes.QuadPart);
                printf("Вільна ємність: %llu байт\n", totalNumberOfFreeBytes.QuadPart);
            }
            else {
                printf("Не вдалося отримати інформацію про вільне місце\n");
            }

            printf("-------------------------------------\n");


        }
    }

    GetSystemMemoryInfo();
    printf("-------------------------------------\n");

    GetComputerNameInfo();
    printf("-------------------------------------\n");

    GetUserNameInfo();
    printf("-------------------------------------\n");

    GetDirectoryInfo();
    printf("-------------------------------------\n");

    return 0;
}
